var compras_data = Object();



/*
 * Objeto para gestionar la informacion de la familia logueada en la aplicacion.
 * familia
 */

familia = {
    
    /*
     * familia.setCompras(data);
     */
    setCompras: function(data, callback){
        callback = callback || function(){};
        compras_data = data;
        familia.loadProductoCompras();
        callback();
    },
    
    /*
     * familia.loadProductoCompras();
     */
    loadProductoCompras: function(callback) { 
        callback = callback || function(){};
        var productos = Object(); 
        var cantidad = Object();
        var valor = Object();
        for (var key in compras_data.data) { 
            var mes = compras_data.data[key].compra_anio+'/'+compras_data.data[key].compra_mes;
            var p = { 
                  producto: compras_data.data[key].compra_producto
                , grupoAlimento: compras_data.data[key].compra_grupoAlimento
                , departamento: compras_data.data[key].compra_departamento
                , municipio: compras_data.data[key].compra_municipio
                , oficina: compras_data.data[key].compra_oficina 
                , unidad: compras_data.data[key].compra_unidad
                , unidadPrefijo: compras_data.data[key].compra_unidadPrefijo
                , cantidad: Array() 
                , valor: Array()
            };
            productos[compras_data.data[key].compra_productoCodigo] = productos[compras_data.data[key].compra_productoCodigo] || p;
            cantidad[compras_data.data[key].compra_productoCodigo] = cantidad[compras_data.data[key].compra_productoCodigo] || Object();
            cantidad[compras_data.data[key].compra_productoCodigo][mes] = parseFloat(cantidad[compras_data.data[key].compra_productoCodigo][mes]) || parseFloat(0);
            cantidad[compras_data.data[key].compra_productoCodigo][mes] += parseFloat(compras_data.data[key].compra_cantidad);
            valor[compras_data.data[key].compra_productoCodigo] = valor[compras_data.data[key].compra_productoCodigo] || Object();
            valor[compras_data.data[key].compra_productoCodigo][mes] = parseFloat(valor[compras_data.data[key].compra_productoCodigo][mes]) || parseFloat(0);
            valor[compras_data.data[key].compra_productoCodigo][mes] += parseFloat(compras_data.data[key].compra_valorTotal);
        }
        for (var a in cantidad) { 
            for (var b in cantidad[a]) { 
                productos[a].cantidad.push([b, cantidad[a][b]]);
            }
        }
        for (var a in valor) { 
            for (var b in valor[a]) { 
                productos[a].valor.push([b, valor[a][b]]);
            }
        }
        compras_data.productos = productos;
        callback();
    }, 
    
    /*
     * familia.getProductoInfo();
     */
    getProductoInfo: function(codigoProducto, callback){
        callback = callback || function(){};
        return compras_data.productos[codigoProducto];
        callback();
    }, 
    
    /*
     * familia.getListaProductos();
     */
    getListaProductos: function(callback){
        callback = callback || function(){};
        var lista = Array();
        for (var codigo in compras_data.productos) { 
            lista.push(codigo);
        }
        return lista;
    }
    
};