<?php
/**********************************************************
 * CLIENTE: PMA Colombia
 * ========================================================
 * 
 * @copyright PMA Colombia 2016
 * @updated 31/03/2016 17:00
 * @version 1
 * @author {Abel Oswaldo Moreno Acevedo} <{moreno.abel@gmail.com}>
 * 
 **********************************************************/

namespace User\Model\Tables;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\TableGateway\Feature;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Debug\Debug;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class CompraTable extends AbstractTableGateway
{
    protected $table = 'compra';

    public function __construct()
    {
        //$this->adapter = $adapter;
        //$this->initialize();
        
        $this->featureSet = new Feature\FeatureSet();
		$this->featureSet->addFeature(new Feature\GlobalAdapterFeature());
		$this->initialize();
    }

    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet->toArray();
    }
    
    public function getFamiliaCompras ($cedula) { 
        $result = Array();        
        $resultSet = $this->select(function (Select $select) use ($cedula) {
        	$select
        	->columns(array(
                    "compra_oficina"
                , "compra_oficinaID"
                , "compra_socio"
                , "compra_socioID"
                , "compra_puntoVenta"
                , "compra_puntoVentaID"
                , "compra_departamento"
                , "compra_departamentoCodigo"
                , "compra_municipio"
                , "compra_municipioCodigo"
                , "compra_documento"
                , "compra_codigoBarras"
                , "compra_grupoAlimento"
                , "compra_grupoAlimentoID"
                , "compra_producto"
                , "compra_productoCodigo"
                , "compra_unidad"
                , "compra_unidadPrefijo"
                , "compra_precioUnidad"
                , "compra_cantidad"
                , "compra_valorTotal"
                , "compra_fecha"
                , 'compra_anio' => new \Zend\Db\Sql\Expression("YEAR(compra_fecha)") 
                , 'compra_mes' => new \Zend\Db\Sql\Expression("MONTH(compra_fecha)")
                , "fam_id" 
                ))
        	->where("compra_documento = '{$cedula}' ");
        });
        $result['data'] = $resultSet->toArray();
        //Debug::dump($result); die;
        return $result;
    }
    
    
    
    
    

}
