<?php
/**********************************************************
 * CLIENTE: PMA Colombia
 * ========================================================
 * 
 * @copyright PMA Colombia 2016
 * @updated 30/03/2016 15:50
 * @version 1
 * @author {Abel Oswaldo Moreno Acevedo} <{moreno.abel@gmail.com}>
 * 
 **********************************************************/

namespace User\Model\Tables;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\TableGateway\Feature;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Debug\Debug;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class FamiliaTable extends AbstractTableGateway
{
    protected $table = 'familia';

    public function __construct()
    {
        //$this->adapter = $adapter;
        //$this->initialize();
        
        $this->featureSet = new Feature\FeatureSet();
		$this->featureSet->addFeature(new Feature\GlobalAdapterFeature());
		$this->initialize();
    }

    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet->toArray();
    }
    
    /**
     * 
     * Devuelve la familia correspondiente a la cedula y 
     * codigo de barras ingresados.
     * 
     * @param string $fam_cedula
     * @param string $fam_cedula
     * @return Ambigous <multitype:, ArrayObject, NULL, \ArrayObject, unknown>|boolean
     */
    public function getFamilia($fam_cedula = '', $fam_codBarras = ''){
    	$params = array('fam_cedula' => $fam_cedula, 'fam_codigoBarras' => $fam_codBarras); 
    	$resultSet = $this->select($params);  
    	if ($resultRow = $resultSet->current()){ 
        	return $resultRow;
    	}else {
    		return false;
    	}
    }
    
    
    

}
