<?php
/**********************************************************
 * CLIENTE: PMA Colombia
 * ========================================================
 * 
 * @copyright PMA Colombia 2016
 * @updated 
 * @version 1
 * @author {Abel Oswaldo Moreno Acevedo} <{moreno.abel@gmail.com}>
 **********************************************************/
namespace User\Model;

use User\Model\Tables\FamiliaTable;
use User\Model\Tables\CompraTable;
use Zend\Debug\Debug;
use Zend\Authentication\Storage;

/**********************************************************
 * MODELO User
 * ======================================================= 
 * 
 *  Extiende de Storage\Session de ZF2 por lo que se tienen
 *  los atributos y metodos de la clase de la cual se
 *  extiende. (Zend\Authentication\Storage\Session.php)
 *  
 * 
 *	ATRIBUTOS
 *	$familiaTable   // Tabla usuarios
 *
 *
 * 	METODOS
 *	login($username = '', $password = '');
 *      logout();
 *      setActiveUser($data);
 *      getActiveUser();
 *      isLogin();
 *      getModules($userId, $parentId);
 *      getPrivileges($userId, $moduleId);
 *  
 **********************************************************/
class Familia extends Storage\Session
{
    protected $familiaTable;
    protected $compraTable;




    /** 
     * Llama al contructor de Storage\Session e instancia
     * userTable.
     */
    public function __construct()
    {
        //  Contructor de la clase 
        parent::__construct();  
        
    	$this->familiaTable = new FamiliaTable();	// Inicializa la tabla
        $this->compraTable = new CompraTable();
    }
    
    /**
     * Recibe la cedula y codigo de barras del jefe de hogat, devuelve verdadero 
     * si el usuario existe.
     * 
     * @param string $cedula
     * @param string $codigoBarras
     * @return boolean
     */
    public function login($cedula = '', $codigoBarras = '')
    {
        $familia = $this->familiaTable->getFamilia($cedula, $codigoBarras);
        if ($familia){ 
            $this->setActiveFamilia($familia);
            return true;
        }
        return false;
    }
    
    
    /**
     * Limpia la sesion
     */
    public function logout(){
        $this->clear();
    }
    
    
    /**
     * Guarda el objeto $data dentro de la sesion del usuario.
     * 
     * @param unknown $data
     */
    public function setActiveFamilia($data){
        $this->write($data);    // Funcion heredada 
    }
    
    
    /**
     * 
     * @return \Zend\Authentication\Storage\mixed
     */
    public function getActiveUser(){
        return $this->read();   // Funcion heredada
    }
    
    
    /**
     * Si no hay elementos en la sesion del usuario devuelve falso,
     * es decir, si la sesion esta vacia devuelve falso y sino 
     * devuelve verdadero.
     * 
     * @return boolean
     */
    public function isLogin(){
        return !$this->isEmpty();   // Funcion heredada
    }
    
    
    public function loadCompras($cedula){
        $compras = $this->compraTable->getFamiliaCompras($cedula);
        //Debug::dump($compras); die;
        return $compras;
    }
    
}

?>