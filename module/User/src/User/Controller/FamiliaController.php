<?php
/**********************************************************
 * CLIENTE: PMA Colombia
 * ========================================================
 * 
 * @copyright PMA Colombia 2014
 * @updated 31/03/2016 17:00
 * @version 1
 * @author {Abel Oswaldo Moreno Acevedo} <{moreno.abel@gmail.com}>
 **********************************************************/

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Model\Familia;
use Util\FamiliaSession;

/**********************************************************
 * CONTROLADOR FamiliaController
 * ======================================================= 
 * 
 * 	METODOS
 *	indexAction();
 *  
 **********************************************************/
class FamiliaController extends AbstractActionController
{


    public function indexAction()
    {
        $familia = new Familia();
        $compras = $familia->loadCompras(\Util\FamiliaSession::getDocumento());
        $viewModel = new ViewModel(array("compras" => json_encode($compras)));
        return $viewModel;
    }
    
    public function eduAction()
    {
        return array();
    }
    
    public function leccionesAction()
    {
        return array();
    }
    
    public function tipAction()
    {
        return array();
    }
	
    
}
