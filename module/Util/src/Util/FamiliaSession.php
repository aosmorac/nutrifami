<?php
namespace Util;

use User\Model\Familia;
use Zend\Http\PhpEnvironment\Request;
use Zend\Debug\Debug;

class FamiliaSession
{
       
    public static function getName() {
        $familia = new Familia();
        if ($familia->isLogin()) {
            $data = $familia->getActiveUser();
            return $data['fam_nombre1'].' '.$data['fam_nombre2'];
        }
        return '';
    }
    
    
    public static function getLastName() {
        $familia = new Familia();
    	if ($familia->isLogin()) {
    		$data = $familia->getActiveUser();
    		return $data['fam_apellido1'].' '.$data['fam_apellido1'];
    	}
    	return '';
    }
    
    
    public static function getFullName() {
    	$familia = new Familia();
    	if ($familia->isLogin()) {
    		$data = $familia->getActiveUser();
    		return $data['fam_nombre1'].' '.$data['fam_nombre2'].' '.$data['fam_apellido1'].' '.$data['fam_apellido1'];
    	}
    	return '';
    }
    
    
    public static function getDocumento() {
    	$familia = new Familia();
    	if ($familia->isLogin()) {
    		$data = $familia->getActiveUser();
                //Debug::dump($data); die;
    		return $data['fam_cedula'];
    	}
    	return '';
    }
    
    
    public static function isLogin(){
        $familia = new Familia;
        return $familia->isLogin();
    }
    
}

?>